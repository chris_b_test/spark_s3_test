package com.mycompany.app;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.apache.spark.sql.SparkSession;
/**
 * Unit test for simple App.
 */
public class AppTest 
{

    @Test
    public void read() {
        var session = SparkSession.builder() //
                .master("local[2]") //
                .appName("exam") //
                .getOrCreate();

        var hadoop_conf = session.sparkContext().hadoopConfiguration();

        hadoop_conf.set("fs.s3a.impl", "org.apache.hadoop.fs.s3a.S3AFileSystem");
        hadoop_conf.set("fs.s3a.path.style.access", "true");
        hadoop_conf.set("fs.s3a.connection.ssl", "true");
        hadoop_conf.set("com.amazonaws.services.s3a.enableV4", "true");

        hadoop_conf.set("fs.s3a.access.key", "update_me");
        hadoop_conf.set("fs.s3a.secret.key", "update_me");

        var y = session.read().csv("s3a://kalenda-test-bucket/requests.csv");
        y.show();

        assertEquals(3, y.columns().length);

        session.stop();
        session.close();

    }
}
